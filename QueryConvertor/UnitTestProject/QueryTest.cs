﻿using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using QueryConvertor.Controllers;
using QueryConvertor.Core.Cache;
using QueryConvertor.Core.Convertors.JsonConvertor;
using QueryConvertor.Core.Convertors.XmlConvertor;
using QueryConvertor.Core.XmlManagement;

namespace UnitTestProject
{
    [TestClass]
    public class QueryTest
    {
        //Mocks if required
        private Mock<IXmlRepository> _xmlRepository;
        private Mock<IJsonConvertor> _jsonConvertor;
        private Mock<IXmlConvertor> _xmlConvertor;

        private Mock<ICacheService> _cache;
        private Mock<IDirectoryBinder> _directoryBinder;
        //Mocks if required

        private QueryController _queryController;

        private IXmlRepository helperRepository;

        //Primary tests configuration and setup
        [TestInitialize]
        public void Initialization()
        {
            //cache
            _cache = new Mock<ICacheService>();

            //set empty cache for any value
            _cache.Setup(c => c.GetFromCache<string>(It.IsAny<string>())).Returns((string)null);
            _cache.Setup(c => c.SetCache(It.IsAny<string>(), It.IsAny<XmlDocument>())).Returns((XmlDocument)null);

            //binder
            _directoryBinder = new Mock<IDirectoryBinder>();

            _directoryBinder.Setup(d => d.GetStorageFolder()).Returns(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + "\\XmlData");

            //Reuse: Xml repository for loading Xml when Mock not sufficient
            helperRepository = new XmlRepository(_cache.Object, _directoryBinder.Object);

            //primary mock setup
            _xmlRepository = new Mock<IXmlRepository>();
            _jsonConvertor = new Mock<IJsonConvertor>();
            _xmlConvertor = new Mock<IXmlConvertor>();

            _queryController = new QueryController(_xmlRepository.Object, _jsonConvertor.Object, _xmlConvertor.Object);
        }


        [TestMethod]
        public void TestXmlRepository_LoadFromDiskIfCacheNull()
        {
            //Arrange
            IXmlRepository xmlRep = new XmlRepository(_cache.Object, _directoryBinder.Object);

            //Act
            XmlDocument doc = xmlRep.GetXmlByName("Name1");

            //Assert
            Assert.IsNotNull(doc);
            Assert.AreEqual(doc.SelectSingleNode("data/filter/AND/OR/AND/*[1]").Attributes["fieldName"].Value, "FieldName3");
        }


        [TestMethod]
        public async Task TestControllerGet()
        {
            //Arrange
            IXmlRepository xmlRep = new XmlRepository(_cache.Object, _directoryBinder.Object);
            IJsonConvertor jsonCovertor = new JsonConvertor();
            IXmlConvertor xmlCovertor = new XmlConvertor();

            QueryController controller = new QueryController(xmlRep, jsonCovertor, xmlCovertor);

            //Act
            JsonResult result = await controller.Get("Name1");

            //Assert
            Assert.IsNotNull(result.Value, "There should be some data for the JsonResult");
            //Assert.AreEqual(result.Value.GetReflectedProperty("page"), );
        }

        
    }

    public static class Helper
    {
        public static object GetReflectedProperty(this object obj, string propertyName)
        {

            PropertyInfo property = obj.GetType().GetProperty(propertyName);

            if (property == null)
            {
                return null;
            }

            return property.GetValue(obj, null);
        }
    }
}
