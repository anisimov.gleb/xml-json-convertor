﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueryConvertor.Core.Cache
{
    /// <summary>
    /// Cache Service
    /// </summary>
    public class CacheService : ICacheService
    {
        private IMemoryCache _cache;

        public CacheService(IMemoryCache cache)
        {
            _cache = cache;
        }

        public T GetFromCache<T>(string cacheItemName)
        {
           return _cache.Get<T>(cacheItemName);
        }


        public T SetCache<T>(string cacheItemName, T obj)
        {
            return _cache.Set<T>(cacheItemName, obj);
        }



        public async Task<T> GetObjectFromCache<T>(string cacheItemName, int cacheTimeInMinutes, Func<Task<T>> objectSettingFunction)
        {
            var cachedObject = await _cache.GetOrCreateAsync<T>(
                cacheItemName,
                c =>
                {
                    c.AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(cacheTimeInMinutes);
                    return objectSettingFunction();
                });

            return cachedObject;

        }

        public T GetObjectFromCache<T>(string cacheItemName, int cacheTimeInMinutes, Func<T> objectSettingFunction)
        {
            throw new NotImplementedException();
        }
    }
}
