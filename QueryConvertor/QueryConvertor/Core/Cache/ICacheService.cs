﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueryConvertor.Core.Cache
{
   public interface ICacheService
    {
        T GetFromCache<T>(string cacheItemName);

        T SetCache<T>(string cacheItemName, T obj);

        /// <summary>
        /// A generic method for getting and setting objects to the memory cache.
        /// </summary>
        /// <typeparam name="T">The type of the object to be returned.</typeparam>
        /// <param name="cacheItemName">The name to be used when storing this object in the cache.</param>
        /// <param name="cacheTimeInMinutes">How long to cache this object for.</param>
        /// <param name="objectSettingFunction">A parameterless function to call if the object isn't in the cache and you need to set it.</param>
        /// <returns>An object of the type you asked for</returns>
        Task<T> GetObjectFromCache<T>(string cacheItemName, int cacheTimeInMinutes, Func<Task<T>> objectSettingFunction);


        /// <summary>
        /// Not Async version ( not complicated calls )
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheItemName"></param>
        /// <param name="cacheTimeInMinutes"></param>
        /// <param name="objectSettingFunction"></param>
        /// <returns></returns>
        T GetObjectFromCache<T>(string cacheItemName, int cacheTimeInMinutes, Func<T> objectSettingFunction);

        

    }
}
