﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using QueryConvertor.Core.Cache;

namespace QueryConvertor.Core.XmlManagement
{
    public class XmlRepository : IXmlRepository
    {
        //TODO: Decouple

        private ICacheService Cache;
        private IDirectoryBinder DirectoryBinder;

        public XmlRepository(ICacheService cache, IDirectoryBinder directoryBinder)
        {
            Cache = cache;
            DirectoryBinder = directoryBinder;
        }

        /// <summary>
        /// Get Full Path to xml
        /// </summary>
        /// <param name="name">xml name</param>
        /// <returns></returns>

        private string GetXmlPath(string name)
        {
            return Path.Combine(DirectoryBinder.GetStorageFolder(), name + ".xml");
        }

        /// <summary>
        /// Get xml file from folder.
        /// </summary>
        /// <param name="name">File Name.</param>
        /// <returns>XmlDocument</returns>
        public XmlDocument GetXmlByName(string name)
        {
            //Cache
            XmlDocument doc = Cache.GetFromCache<XmlDocument>(name);

            if (doc == null)
            {
                doc = new XmlDocument();
                doc.Load(GetXmlPath(name));
                return doc;
            };

            return doc;
        }

      
        public async Task<XmlDocument> GetXmlByNameAsync(string name)
        {
            return await Task.Run(() => GetXmlByName(name));
        }


        /// <summary>
        /// Get xml file from folder. (Dont read sober)
        /// </summary>
        /// <param name="name">File Name.</param>
        /// <returns>XmlDocument</returns>
        //public async XmlDocument GetXmlByName(string name)
        //{

        //    XmlDocument document = await Cache.GetObjectFromCache<XmlDocument>(name, 15, async () =>
        //    {
        //        XmlDocument doc = new XmlDocument();
        //        return await Task.Run(() =>
        //        {
        //            doc.Load(GetXmlPath(name));
        //            return doc;
        //        });
        //    });

        //    return document;
        //}

        /// <summary>
        /// Save XmlDocument to file by Name.
        /// </summary>
        /// <param name="name">Xml file name</param>
        /// <param name="document">XmlDocument to save</param>


        public void SaveXmlByName(string name, XmlDocument document)
        {
            //try catch
            document.Save(GetXmlPath(name));

            //refresh cache
            Cache.SetCache<XmlDocument>(name, document);
        }

        public async Task SaveXmlByNameAsync(string name, XmlDocument document)
        {
            await Task.Run(() => SaveXmlByName(name, document));
        }


    }
}
