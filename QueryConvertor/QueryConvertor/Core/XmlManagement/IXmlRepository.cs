﻿using QueryConvertor.Core.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace QueryConvertor.Core.XmlManagement
{
    public interface IXmlRepository
    {
        XmlDocument GetXmlByName(string name);

        Task<XmlDocument> GetXmlByNameAsync(string name);

        void SaveXmlByName(string name, XmlDocument document);

        Task SaveXmlByNameAsync(string name, XmlDocument document);
    }
}
