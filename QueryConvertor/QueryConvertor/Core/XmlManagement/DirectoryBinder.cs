﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace QueryConvertor.Core.XmlManagement
{
    public class DirectoryBinder : IDirectoryBinder
    {
        private string _directory;

        public string GetStorageFolder()
        {
            if (string.IsNullOrEmpty(_directory))
            {
                string directory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

                int binIndex = directory.IndexOf("\\bin");

                _directory = directory.Remove(binIndex, directory.Length - binIndex) + "\\XmlData\\"; //should be stored in appsettings.json
            }
           
            return _directory;
        }
    }
}
