﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueryConvertor.Core.XmlManagement
{
    public interface IDirectoryBinder
    {
        string GetStorageFolder();
    }
}
