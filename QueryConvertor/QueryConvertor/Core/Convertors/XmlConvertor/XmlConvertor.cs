﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using QueryConvertor.Core.Convertors.QueryModel;

namespace QueryConvertor.Core.Convertors.XmlConvertor
{
    public class XmlConvertor : IXmlConvertor
    {
        /// <summary>
        /// Parse From XML To ObjectModel
        /// </summary>
        /// <param name="doc">xml document</param>
        /// <returns>Query Object Model</returns>
        public QueryObjectModel ParseToModel(XmlDocument doc)
        {
            QueryObjectModel model = new QueryObjectModel();

            XmlNode filterNode = doc.DocumentElement.SelectSingleNode("/data/filter");

            // Load xml document.
            FilterStatement root = new FilterStatement();
            ParseToModelRecursion(filterNode.ChildNodes, root);

            //Because "filter" will contain only one root ( and/or )
            model.Filter.Statement = root.InnerStatements[0];

            #region Sort

            XmlNode sortNode = doc.DocumentElement.SelectSingleNode("/data/sort");


            foreach (XmlNode child in sortNode.ChildNodes)
            {
                SortStatement ss = new SortStatement();

                ss.FieldName = child.Attributes["name"].Value;
                ss.Accending = bool.Parse(child.Attributes["ascending"].Value);

                model.Sort.Statements.Add(ss);
            }

            #endregion Sort

            return model;
        }

        /// <summary>
        /// Build XML from QueryObjectModel
        /// </summary>
        /// <param name="model">QueryObjectModel</param>
        /// <returns>XML Document</returns>
        public XmlDocument BuildFromModel(QueryObjectModel model)
        {
            XmlDocument doc = new XmlDocument();

            doc.LoadXml("<data><filter></filter><sort></sort></data>");

            //Filter
            XmlNode filterNode = doc.SelectSingleNode("/data/filter");

            BuildFromModelRecursion(model.Filter.Statement, filterNode, doc);

            //Sort
            XmlNode sortNode = doc.SelectSingleNode("/data/sort");

            foreach (var statement in model.Sort.Statements)
            {
                XmlElement newSortNode = doc.CreateElement("field");

                newSortNode.SetAttribute("name", statement.FieldName);
                newSortNode.SetAttribute("ascending", statement.Accending.ToString().ToLower());

                sortNode.AppendChild(newSortNode);
            }
            

            return doc;

        }

        private void BuildFromModelRecursion(FilterStatement statement, XmlNode rootNode, XmlDocument doc)
        {
            XmlElement booleanOperator = doc.CreateElement(statement.BooleanOperator.ToString());

            //if statement has InnerConditions Use recursive call
            if (statement.InnerStatements.Any())
            {
                foreach (var ic in statement.InnerStatements)
                {
                    BuildFromModelRecursion(ic, booleanOperator, doc);

                    rootNode.AppendChild(booleanOperator);
                }
            }

            //if statement has Conditions
            //add them one by one
            if (statement.Conditions.Any())
            {
                foreach (var c in statement.Conditions)
                {
                    XmlElement newFilterCondition = doc.CreateElement("condition");

                    newFilterCondition.SetAttribute("fieldName", c.FieldName);
                    newFilterCondition.SetAttribute("operator", c.CompareOperator); //System.Net.WebUtility.HtmlEncode not required
                    newFilterCondition.SetAttribute("value", c.Value);
                    newFilterCondition.SetAttribute("valueType", c.ValueType.ToString());

                    booleanOperator.AppendChild(newFilterCondition);
                }
                rootNode.AppendChild(booleanOperator);
            }
        }

        

        private static void ParseToModelRecursion(XmlNodeList nodes, FilterStatement root)
        {
            foreach (XmlNode node in nodes)
            {
                //if node is Statement with Boolean Operation.
                if (BooleanOperatorEnum.GetNames(typeof(BooleanOperatorEnum)).Contains(node.Name))
                {
                    FilterStatement innerStatement = new FilterStatement();
                    innerStatement.BooleanOperator = EnumHelper.GetByString<BooleanOperatorEnum>(node.Name);

                    // Do something with the node.
                    ParseToModelRecursion(node.ChildNodes, innerStatement);

                    root.InnerStatements.Add(innerStatement);

                }
                else // if node is Condition
                {
                    Condition c = new Condition();
                    c.FieldName = node.Attributes["fieldName"].Value;
                    c.CompareOperator = node.Attributes["operator"].Value;
                    c.Value = node.Attributes["value"].Value;
                    c.ValueType = EnumHelper.GetByString<ConditionValueTypesEnum>(node.Attributes["valueType"].Value);

                    root.Conditions.Add(c);
                }
            }
        }
    }
}
