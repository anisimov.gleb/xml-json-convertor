﻿using QueryConvertor.Core.Convertors.QueryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace QueryConvertor.Core.Convertors.XmlConvertor
{
    public interface IXmlConvertor
    {
        /// <summary>
        /// Parse From XML To ObjectModel
        /// </summary>
        /// <param name="doc">xml document</param>
        /// <returns>Query Object Model</returns>
        QueryObjectModel ParseToModel(XmlDocument doc);

        /// <summary>
        /// Build XML from QueryObjectModel
        /// </summary>
        /// <param name="model">QueryObjectModel</param>
        /// <returns>XML Document</returns>
        XmlDocument BuildFromModel(QueryObjectModel model);
    }
}
