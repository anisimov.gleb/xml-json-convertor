﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueryConvertor.Core.Convertors.QueryModel
{
    /// <summary>
    /// Query Object Model
    /// </summary>
    public class QueryObjectModel
    {

        public QueryObjectModel()
        {
            Filter = new FilterNode();
            Sort = new SortNode();
        }
        /// <summary>
        /// Filter
        /// </summary>
        public FilterNode Filter { get; set; }

        /// <summary>
        /// Sort
        /// </summary>
        public SortNode Sort { get; set; }
    }
}
