﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueryConvertor.Core.Convertors.QueryModel
{
    public class SortStatement
    {
        public string FieldName { get; set; }
        public bool Accending { get; set; }
    }
}
