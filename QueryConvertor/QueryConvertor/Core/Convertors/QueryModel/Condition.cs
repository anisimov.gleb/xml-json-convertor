﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueryConvertor.Core.Convertors.QueryModel
{
    /// <summary>
    /// Condition of the Query
    /// </summary>
    public class Condition
    {
        /// <summary>
        /// Field Name
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// Compare Operator
        /// </summary>
        public string CompareOperator { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// ValueType ( Text , Array ...)
        /// </summary>
        public ConditionValueTypesEnum ValueType {get;set;}
    }
}
