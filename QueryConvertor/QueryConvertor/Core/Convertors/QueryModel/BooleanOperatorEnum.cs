﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueryConvertor.Core.Convertors.QueryModel
{
    /// <summary>
    /// Boolean Operator
    /// </summary>
    public enum BooleanOperatorEnum
    {
        AND = 1,
        OR = 2
    }
}
