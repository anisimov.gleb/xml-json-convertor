﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueryConvertor.Core.Convertors.QueryModel
{
    /// <summary>
    /// Filter Root Node
    /// </summary>
    public class FilterNode
    {
        public FilterStatement Statement {get;set;}
    }
}
