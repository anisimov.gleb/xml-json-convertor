﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace QueryConvertor.Core.Convertors.QueryModel
{
    public enum ConditionValueTypesEnum
    {
        Text = 1,
        Array = 2,
        FieldName = 3,
        Expression = 4
    }

    //TODO Refactor as helper
    public static class EnumHelper {

        public static T GetByString<T>(string name)
        {
            return (T) Enum.Parse(typeof(T), name);
        }
    }
}
