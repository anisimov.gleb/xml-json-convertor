﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueryConvertor.Core.Convertors.QueryModel
{
    public class SortNode
    {
        public SortNode()
        {
            Statements = new List<SortStatement>();
        }

        public List<SortStatement> Statements;
    }
}
