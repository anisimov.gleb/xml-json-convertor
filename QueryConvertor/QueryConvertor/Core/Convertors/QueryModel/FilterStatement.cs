﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueryConvertor.Core.Convertors.QueryModel
{
    /// <summary>
    /// Filter statements in filter root node ( AND/OR )
    /// </summary>
    public class FilterStatement
    {

        public FilterStatement()
        {
            Conditions = new List<Condition>();
            InnerStatements = new List<FilterStatement>();
        }
        /// <summary>
        /// Operator for conditions
        /// </summary>
        public BooleanOperatorEnum? BooleanOperator;

        /// <summary>
        /// Current level conditions ( leafs )
        /// </summary>
        public List<Condition> Conditions;

        /// <summary>
        /// Children
        /// </summary>
        public List<FilterStatement> InnerStatements; 
    }
}
