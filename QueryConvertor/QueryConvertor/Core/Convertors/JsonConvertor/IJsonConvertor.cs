﻿using Newtonsoft.Json.Linq;
using QueryConvertor.Core.Convertors.QueryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace QueryConvertor.Core.Convertors.JsonConvertor
{
    public interface IJsonConvertor 
    {
        /// <summary>
        /// Parse Json object to QueryObjectModel
        /// </summary>
        /// <param name="jsonObject">json</param>
        /// <returns>QueryObjectModel</returns>
        QueryObjectModel ParseToModel(JObject jsonObject);

        /// <summary>
        /// Build Json object from  QueryObjectModel
        /// </summary>
        /// <param name="model">Object Model</param>
        /// <returns>JObject</returns>
        JObject BuildFromModel(QueryObjectModel model);
    }
}
