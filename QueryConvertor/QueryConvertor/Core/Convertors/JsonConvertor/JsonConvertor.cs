﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using QueryConvertor.Core.Convertors.QueryModel;
using System.Security;
using QueryConvertor.Core.Helper;

namespace QueryConvertor.Core.Convertors.JsonConvertor
{
    public class JsonConvertor : IJsonConvertor
    {
        /// <summary>
        /// Parse Json object to QueryObjectModel
        /// </summary>
        /// <param name="jsonObject">json</param>
        /// <returns>QueryObjectModel</returns>
        public QueryObjectModel ParseToModel(JObject jsonObject)
        {
            QueryObjectModel model = new QueryObjectModel();

            //Parse json into FilterStatement
            JToken jsonFilter = jsonObject["filter"];
            FilterStatement filter = new FilterStatement();

            ParseToModelRecursion(jsonFilter, filter);
            
            model.Filter.Statement = filter.InnerStatements[0];


            //Parse json into SortStatements
            JToken jsonSort = jsonObject["sort"];


            foreach (JToken jt in jsonSort.Children())
            {
                SortStatement ss = new SortStatement();

                ss.FieldName = jt["name"].Value<string>();
                ss.Accending = jt["ascending"].Value<bool>();

                model.Sort.Statements.Add(ss);
            }

            return model;

        }

        /// <summary>
        /// Build Json object from  QueryObjectModel
        /// </summary>
        /// <param name="model">Object Model</param>
        /// <returns>JObject</returns>
        public JObject BuildFromModel(QueryObjectModel model)
        {
            JObject obj = new JObject();

            //Build json from FilterStatement 

            JObject jsonFilter = new JObject();
            obj["filter"] = jsonFilter;

            BuildFromModelRecursion(model.Filter.Statement, jsonFilter);

            //Build json from SortStatements 

            JArray sortArray = new JArray();
            obj["sort"] = sortArray;

            foreach (var statement in model.Sort.Statements)
            {
                JObject sortStatement = new JObject();
                sortStatement.Add("name", statement.FieldName);
                sortStatement.Add("ascending", statement.Accending.ToString().ToLower());

                sortArray.Add(sortStatement);
            }

            return obj;
        }

        /// <summary>
        /// Parse Json node recursively
        /// </summary>
        /// <param name="jsonNode"> Root Json node</param>
        /// <param name="root"> Root FilterStatement to parse into </param>
        private void ParseToModelRecursion(JToken jsonNode, FilterStatement root)
        {

            //if node is Statement with Boolean Operation.
            if (jsonNode["booleanOperator"] != null)
            {
                FilterStatement innerStatement = new FilterStatement();
                innerStatement.BooleanOperator = EnumHelper.GetByString<BooleanOperatorEnum>(jsonNode["booleanOperator"].Value<string>());

                foreach (var c in jsonNode["conditions"])
                {
                    ParseToModelRecursion(c, innerStatement);
                }

                root.InnerStatements.Add(innerStatement);

            }
            else // if node is Condition
            {
                Condition c = new Condition();
                c.FieldName = jsonNode["fieldName"].Value<string>();
                c.CompareOperator = jsonNode["operator"].Value<string>();
                c.Value = jsonNode["value"].Value<string>();
                c.ValueType = EnumHelper.GetByString<ConditionValueTypesEnum>(jsonNode["valueType"].Value<string>());

                root.Conditions.Add(c);
            }
        }

        /// <summary>
        /// Build Json object recursively
        /// </summary>
        /// <param name="statement"></param>
        /// <param name="rootNode"></param>
        private void BuildFromModelRecursion(FilterStatement statement, JObject rootNode)
        {
            JArray array = new JArray();

            //Inner statements
            if (statement.InnerStatements.Any())
            {
                foreach (var ic in statement.InnerStatements)
                {
                    JObject innerStatement = new JObject();

                    BuildFromModelRecursion(ic, innerStatement);

                    array.Add(innerStatement);
                }
            }

            //Conditions without inner statements will close recursion
            //For each condition add them into array
            if (statement.Conditions.Any())
            {
                foreach (var c in statement.Conditions)
                {

                    JObject newFilterCondition = new JObject();

                    newFilterCondition.Add("fieldName", c.FieldName);
                    newFilterCondition.Add("operator", c.CompareOperator);// decode &gt; ... for json
                    newFilterCondition.Add("value", c.Value);
                    newFilterCondition.Add("valueType", c.ValueType.ToString());

                    array.Add(newFilterCondition);
                }
            }

            rootNode.Add("booleanOperator", statement.BooleanOperator.ToString());
            rootNode.Add("conditions", array);
        }
    }
}
