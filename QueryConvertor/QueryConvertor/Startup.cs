﻿
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using QueryConvertor.Core.Cache;
using QueryConvertor.Core.Convertors.JsonConvertor;
using QueryConvertor.Core.Convertors.XmlConvertor;
using QueryConvertor.Core.XmlManagement;

namespace QueryConvertor
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddResponseCaching();
            services.AddMvc(options =>
            {
                options.CacheProfiles.Add("Default",
                    new CacheProfile()
                    {
                        Duration = 60
                    });
                options.CacheProfiles.Add("Never",
                    new CacheProfile()
                    {
                        Location = ResponseCacheLocation.None,
                        NoStore = true
                    });
            });
            
            services.AddMemoryCache();
            

            // Add application services.
            services.AddTransient<IXmlRepository, XmlRepository>();
            services.AddTransient<IXmlConvertor, XmlConvertor>();
            services.AddTransient<IJsonConvertor, JsonConvertor>();
            services.AddTransient<ICacheService, CacheService>();

            services.AddSingleton<IDirectoryBinder, DirectoryBinder>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseResponseCaching();
            app.UseMvc();
            
        }
    }
}
