﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using QueryConvertor.ApiModels;
using QueryConvertor.Core.Convertors.JsonConvertor;
using QueryConvertor.Core.Convertors.QueryModel;
using QueryConvertor.Core.Convertors.XmlConvertor;
using QueryConvertor.Core.XmlManagement;

namespace QueryConvertor.Controllers
{
    [Route("api/[controller]")]
    public class QueryController : Controller
    {
        private IXmlRepository _xmlRepository;
        
        private IJsonConvertor _jsonConvertor;
        private IXmlConvertor _xmlConvertor;


        public QueryController(IXmlRepository xr, IJsonConvertor jc, IXmlConvertor xc)
        {
            _xmlRepository = xr;
            _jsonConvertor = jc;
            _xmlConvertor = xc;
        }

        // GET api/Get
        [HttpGet]
        [ResponseCache(Duration = 30, CacheProfileName = "Default", VaryByQueryKeys = new string[] {"name"})] // Json cache for 30 sec
        public async Task<JsonResult> Get(string name)
        {
            //Get from repos
            XmlDocument doc = await _xmlRepository.GetXmlByNameAsync(name);

            //Parse
            QueryObjectModel model = _xmlConvertor.ParseToModel(doc);

            //Build response
            JObject json = _jsonConvertor.BuildFromModel(model);

            return Json(json);
        }


        // POST api/Post
        [HttpPost]
        public async Task<JsonResult> Post([FromBody]SaveJsonApiModel model)
        {
            // parse
            JObject jsonObject = JObject.Parse(model.Json); 

            //Convert
            QueryObjectModel objectModel = _jsonConvertor.ParseToModel(jsonObject);
            XmlDocument docToSave = _xmlConvertor.BuildFromModel(objectModel);

            //save
            await _xmlRepository.SaveXmlByNameAsync(model.Name, docToSave);

            //return Ok();
            return Json(new { Status = "OK" });
           
        }
    }
}
