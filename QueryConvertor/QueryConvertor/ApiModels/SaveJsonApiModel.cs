﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueryConvertor.ApiModels
{
    public class SaveJsonApiModel
    {
        public string Name { get; set; }

        public string Json { get; set; }
    }
}
